# wxJForm

Create simple UI Forms using a JSON Schema.  This application functions similar
to zenity, but allows you to build your UI using a JSON schema.



# Downloads

The following downloads are provided. You may need to `chmod +x` the file before
you can run it.

| Branch | Status | Download |
| ------ | ------ | -------- |
| dev    | [![pipeline status](https://gitlab.com/GavinNL/wxJForm/badges/dev/pipeline.svg)](https://gitlab.com/GavinNL/wxJForm/-/commits/dev)       | [ Ubuntu 18.04](https://gitlab.com/GavinNL/wxJForm/-/jobs/artifacts/dev/raw/artifacts/wxJForm?job=build-bionic) <br> [ Ubuntu 20.04](https://gitlab.com/GavinNL/wxJForm/-/jobs/artifacts/dev/raw/artifacts/wxJForm?job=build-focal)         |
| master | [![pipeline status](https://gitlab.com/GavinNL/wxJForm/badges/master/pipeline.svg)](https://gitlab.com/GavinNL/wxJForm/-/commits/master)       | [ Ubuntu 18.04](https://gitlab.com/GavinNL/wxJForm/-/jobs/artifacts/master/raw/artifacts/wxJForm?job=build-bionic) <br> [ Ubuntu 20.04](https://gitlab.com/GavinNL/wxJForm/-/jobs/artifacts/master/raw/artifacts/wxJForm?job=build-focal)         |










## Purpose

I needed a way to quickly interact with a project but didn't want to build a
specific UI for it. Rather I wanted to be able to send JSON messages to my
application using standard input. So I developed this application based on https://rjsf-team.github.io/react-jsonschema-form/.

Simply build the UI using a JSON schema and then pipe that data to wxJForm. It
will produce a UI which you can use to set the values.



```Bash
cat samples/numbers.json | wxJForm --json
```
![alt text](images/numbers.png "Logo Title Text 1")

Pressing OK will print out the JSON data to standard output.
```
{
    "float_range": 0.4,
    "integer_range": 0,
    "standard_float": 3.145,
    "standard_integer": 66
}

```

Without the `--json` flag, the values will be printed to standard out as
bash variables.

```
float_range=0.4
integer_range=0
standard_float=3.145
standard_integer=66
```

We can then execute the following using the `--export` and `--prefix`
flag to export some variables with the prefix MYVAR_

```bash
$ eval $( cat samples/numbers.json | wxJForm --export --prefix MYVAR_)
$ env | grep MYVAR
MYVAR_standard_integer=66
MYVAR_float_range=0.283379
MYVAR_integer_range=2
MYVAR_standard_float=3.145

```

# Real Time Data Viewing

You can also view real-time output from an application's standard out. The application needs to print JSON objects to standard out. This can be piped into wxJForm to produce a real time view.

An example application `generator.sh` is provided to output sample data. It simply uses curl to get data from a REST API.

```Bash

./generator.sh | wxJForm --view

```

![alt text](images/view.gif "Logo Title Text 1")


# Building

## Dependencies

 * https://github.com/wxWidgets/wxWidgets
 * https://github.com/nlohmann/json


A `conanfile.txt` is provided to handle the dependencies. You will need to
include the bincrafter's repository

```bash
# Install conan if you don't already have it
pip3 install --user conan

conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

Then to compile the application:

```Bash
cd wxJForm
mkdir build && cd build
conan install .. -s compiler.libcxx=libstdc++11 --build missing
cmake .. -DCMAKE_BUILD_TYPE=Release
make

```

# Create A Simple Form

There are a few samples in the `samples` folder which explain how to build various
widgets

```Bash
cat samples/numbers.json | wxJForm
```
![alt text](images/numbers.png "Logo Title Text 1")


# JSON Types

## Strings

```Bash
cat samples/strings.json | wxJForm
```
![alt text](images/strings.png "Strings")

### String Properties

| Property      | Type             | Description                                    |
| ------------- | ---------------- | ---------------------------------------------- |
| `"type"`      | string           | Must be set to `"string"`                      |
| `"enum"`      | array of strings | If set, will turn the widget into a combo box. |
| `"ui:widget"` | string           | One of `"color"`, `"dir"`, `"file"`, `"date"`  |


## Numbers

```Bash
cat samples/numbers.json | wxJForm
```

![alt text](images/numbers.png "Numbers")

### Number Properties

| Property      | Type   | Description                             |
| ------------- | ------ | --------------------------------------- |
| `'"type"`'    | string | One of '"integer"' or `"number"`        |
| `"ui:widget"` | string | One of `"range"` or `"updown"`(default) |
| `"minimum"`   | number | minimum value for the numbers           |
| `"maximum"`   | number | maximum value for the numbers           |

```
{
    "type" : "number",
    "ui:widget" : "range",
    "minimum" : 0,
    "maximun: " 100
}
```

## Boolean

```Bash
cat samples/boolean.json | wxJForm
```

![alt text](images/boolean.png "Numbers")

### Boolean Properties

| Property    | Type    | Description                    |
| ----------- | ------- | ------------------------------ |
| `'"type"`'  | string  | Must be '"boolean"'            |
| `"default"` | boolean | The initial value of the field |


```
{
    "type" : "boolean",
    "title" : "Boolean Field",
    "default" : true
}
```


## Arrays

```Bash
cat samples/arrays.json | wxJForm
```

```
{
    "type" : "array",
    "items" : [
        {
            "type" : "string",
            "default" : "hello"
        },
        {
            "type" : "string",
            "default" : "world"
        }        
    ],
    "additionalItems" : [
      {
          "type" : number,
          "title" : "Number"
      },
      {
        "type" : string,
        "title" : "String"
      }
    ]
}
```

![alt text](images/arrays.png "Arrays")

### Array Properties

| Property            | Type             | Description                                                                                         |
| ------------------- | ---------------- | --------------------------------------------------------------------------------------------------- |
| `"items"`           | array of objects | A list of JSON Shema objects                                                                        |
| `"additionalItems"` | array of objects | A list of JSON Shema objects. These items can be chosen from by the user when adding/removing items |

 **Notes**:
 * If `"additionalItems"` is not set. Then the array size is fixed.
 * If `"additionalItems"` is an array of a single object, then the ComboBox of items is not shown

## Objects

| Property      | Type    | Description                                                                            |
| ------------- | ------- | -------------------------------------------------------------------------------------- |
| `"ui:visble"` | boolean | Shows/Hides the property in the UI. The property will be visible in the final document |
| `"ui:width"`  | number  | Width of the root window                                                               |
| `"ui:height"` | number  | height of the root window                                                              |
| `"title"`     | string  | Alternative text for the key, or window title if root                                  |
