/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>

#include <nlohmann/json.hpp>

JsonArrayWidget::JsonArrayWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    {
        wxBoxSizer* bSizer1;
        bSizer1 = new wxBoxSizer( wxVERTICAL );


        wxBoxSizer* bSizer2;
        bSizer2 = new wxBoxSizer( wxVERTICAL );


        bSizer1->Add( bSizer2, 1, wxEXPAND, 5 );

        wxBoxSizer* bSizer3;
        bSizer3 = new wxBoxSizer( wxHORIZONTAL );


       // bSizer3->Add( 0, 0, 1, wxEXPAND, 5 );


        m_ItemsBox = new wxChoice(this, wxID_ANY);

        bSizer3->Add(m_ItemsBox, 1, wxALL, 5);

        m_Remove = new wxButton( this, wxID_ANY, wxT("-"), wxDefaultPosition, wxDefaultSize, 0 );
        m_Remove->SetBackgroundStyle(wxBG_STYLE_PAINT);
        m_Remove->SetBackgroundColour( wxColour( 255, 0, 6 ) );
        //m_button10->SetMaxSize( wxSize(30,30) );
        //m_button10->SetMinSize( wxSize(30,30) );

        bSizer3->Add( m_Remove, 0, wxALL, 5 );

        m_Add = new wxButton( this, wxID_ANY, wxT("+"), wxDefaultPosition, wxDefaultSize, 0 );
        m_Add->SetBackgroundStyle(wxBG_STYLE_PAINT);
        m_Add->SetBackgroundColour( wxColour( 0, 203, 68 ) );
        //m_button11->SetMaxSize( wxSize(30,30) );
        //m_button11->SetMinSize( wxSize(30,30) );
        bSizer3->Add( m_Add, 0, wxALL, 5 );


        bSizer1->Add( bSizer3, 0, wxEXPAND, 5 );

        m_Add->Bind(wxEVT_BUTTON, &JsonArrayWidget::onAdd, this);
        m_Remove->Bind(wxEVT_BUTTON, &JsonArrayWidget::onRemove, this);

        this->SetSizer( bSizer1 );
        this->Layout();
        Centre();
        m_ItemSizer = bSizer2;
        m_AddRemove = bSizer3;
        return;
    }
}


void JsonArrayWidget::onAdd(wxCommandEvent& event)
{
    auto ind = m_ItemsBox->GetSelection();
    //if( m_additionalItem.is_object() )
    {
        auto sp = new JsonValueWidget(this);
        sp->from_schema(m_additionalItems.at(ind));
        pushBack(sp);

        wxWindow* parent=GetParent();
        while(parent != nullptr)
        {
            parent->Layout();
            parent = parent->GetParent();
        }
    }
}
void JsonArrayWidget::onRemove(wxCommandEvent& event)
{
    popBack();
    wxWindow* parent=GetParent();
    while(parent != nullptr)
    {
        parent->Layout();
        parent = parent->GetParent();
    }
}

void JsonArrayWidget::clear()
{
    while( size() )
    {
        popBack();
    }
}

size_t JsonArrayWidget::size() const
{
    return m_jsonWidgets.size();
}


void JsonArrayWidget::setAdditional(nlohmann::json const & J)
{
    if( J.is_null())
    {
        m_AddRemove->Show(false);
    }
    else
    {
        m_AddRemove->Show(true);
    }
}


void JsonArrayWidget::setValue(nlohmann::json const & J)
{
    clear();
    if( J.is_array() )
    {
        for(size_t i=0;i<J.size();i++)
        {
            pushBack( J.at(i) );
        }
    }
    if( m_additionalItems.is_null())
    {
        m_AddRemove->Show(false);
    }
}

void JsonArrayWidget::pushBack(nlohmann::json const &J)
{
    auto sp = new JsonValueWidget(this);
    sp->setValue(J);
    pushBack(sp);
}

void JsonArrayWidget::pushBack( JsonValueWidget*  ob )
{
    m_ItemSizer->Add(ob, 0, wxEXPAND);
    m_jsonWidgets.push_back(ob);
}

void JsonArrayWidget::popBack()
{
    if( m_jsonWidgets.size())
    {
        m_jsonWidgets.back()->Destroy();
        m_jsonWidgets.pop_back();
    }
}




void JsonArrayWidget::from_schema(const nlohmann::json &Ob)
{
    this->SetAutoLayout(true);

    if( Ob.is_object() )
    {
        if( Ob.count("items"))
        {
            auto & Jitems = Ob.at("items");
            m_additionalItems = nlohmann::json();

            if( Jitems.is_array() )
            {
                for(auto & E : Jitems)
                {
                    auto sp = new JsonValueWidget(this);
                    sp->from_schema(E);
                    pushBack(sp);
                }
            }
        }

        if( Ob.count("additionalItems"))
        {
            auto & aI  = Ob.at("additionalItems");

            if( aI.is_object() )
            {
                m_additionalItems = nlohmann::json::array();
                m_additionalItems.push_back(aI);
            }
            if( aI.is_array() )
            {
                m_additionalItems = aI;
            }

            if( m_additionalItems.is_array() )
            {
                uint32_t ind=0;
                for(auto &a : m_additionalItems)
                {
                    auto label = a.at("title").get<std::string>();
                    m_ItemsBox->Insert(label,ind++);
                }
                m_ItemsBox->SetSelection(0);
                if( ind == 1)
                {
                    m_AddRemove->Clear(false);

                    m_AddRemove->Add( 0, 0, 1, wxEXPAND, 5 );
                    m_AddRemove->Add( m_Remove,  1, wxEXPAND, 5 );
                    m_AddRemove->Add( m_Add,  1, wxEXPAND, 5 );
                }
            }
            m_AddRemove->Show(true);
        }
        else
        {
            m_AddRemove->Show(false);
        }

        this->Layout();
        return;
    }
    throw std::runtime_error("Not a valid schema");
}



nlohmann::json JsonArrayWidget::to_json() const
{
    nlohmann::json J = nlohmann::json::array();
    for(auto s : m_jsonWidgets)
    {
        J.push_back( s->to_json() );
    }
    return J;
}
