/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#ifndef WX_JFORM_H
#define WX_JFORM_H

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <nlohmann/json.hpp>
#include <fstream>
#include <limits>

#include <wx/collpane.h>

class JsonValueWidget;

class JsonValue
{
public:
    /**
     * @brief to_json
     * @return
     *
     * Returns the json object that represents the data
     */
    virtual nlohmann::json to_json() const = 0;

    /**
     * @brief from_schema
     * @param J
     *
     * Builds the wiget based on the Json Schema. The Schema
     * must be a valid schema. Each class that inhreits
     * from this must implement this method
     */
    virtual           void from_schema(nlohmann::json const &J) = 0;

    /**
     * @brief setValue
     * @param J
     *
     * Sets the value of the class using a json value.
     */
    virtual           void setValue(nlohmann::json const & J) = 0;



    /**
     * @brief dereference
     * @param obj
     * @param rootDocument
     * @return
     *
     * Given an object which contains a "$ref" property and teh root document,
     * return a fully realized json object with no "$ref"s
     */
    static void dereference(  nlohmann::json & obj, nlohmann::json const& rootDocument)
    {
        if( obj.count("$ref"))
        {
            auto p = getRef( rootDocument, obj.at("$ref").get<std::string>() );

            if( p != nullptr) // we found the reference
            {
                // make a copy of the original object, but remove the "$ref" key
                nlohmann::json oldValue = obj;
                oldValue.erase("$ref");

                // copy the ref object over
                obj = *p;

                // now add all the original keys back in
                for(auto it = oldValue.begin(); it!=oldValue.end(); it++)
                {
                    obj[it.key()] = std::move( it.value() );
                }
            }
        }
        if( obj.is_object())
        {
            // now dereference any keys for hti sobject
            for(auto it = obj.begin(); it!=obj.end(); it++)
            {
                //std::cout << "Dereferenceing: " << it.key() << std::endl;
                dereference(it.value(), rootDocument);
            }
        }
        if( obj.is_array() )
        {
            for(auto & v : obj )
            {
                dereference(v, rootDocument);
            }
        }
    }

    /**
     * @brief getRef
     * @param rootDocument
     * @param ref
     * @return
     *
     * returns a const reference to the refereced object
     */
    static nlohmann::json const * getRef( nlohmann::json const & rootDocument, std::string ref )
    {
        // "$ref": "#/$defs/enabledToggle"
        auto path = split(ref, "/");
        if( path.size() < 2 ) return nullptr;

        nlohmann::json const * p = &rootDocument;
        for(size_t i=1;i<path.size();i++)
        {
            if( p->count(path[i]) ==0) return nullptr;
            p = &p->at( path[i] );
        }
        return p;
    }


    static std::vector<std::string> split(std::string str, std::string token)
    {
        using namespace std;
        vector<string>result;
        while(str.size())
        {
            auto index = str.find(token);
            if(index!=string::npos){
                result.push_back(str.substr(0,index));
                str = str.substr(index+token.size());
                if(str.size()==0)result.push_back(str);
            }else{
                result.push_back(str);
                str = "";
            }
        }
        return result;
    }
};

/**
 * @brief The JsonNumberWidget class
 *
 * This class handles all number related types. It can
 * come in different styles depending the use-case.
 * The default style is DOUBLE_SPINBOX
 *
 */
class JsonNumberWidget : public JsonValue, public wxWindow
{
    wxWindow    *m_widget = nullptr;
    bool m_isFloat=false;
    double minF = 0.0;
    double maxF = 100.0;

    enum jsonNumberStyle {
        DOUBLE_SPINBOX,
        INT_SPINBOX,
        DOUBLE_SLIDER,
        INT_SLIDER
    };

public:

    JsonNumberWidget(wxWindow * parent);

    void           from_schema(nlohmann::json const & J ) override;
    nlohmann::json to_json() const override;
    void            setValue(nlohmann::json const & J) override;

    void setStyle( jsonNumberStyle s);
    void setRange(double m, double M);
};


/**
 * @brief The JsonStringWidget class
 *
 * This class handles all string related json values.
 * It comes in a variety of styles, the default being TEXTBOX
 */
class JsonStringWidget : public JsonValue, public wxWindow
{
    wxWindow *m_widget = nullptr;
public:

    enum jsonStringStyle {
        TEXTBOX,
        DIR_PATH,    // allows to select a directory
        FILE_PATH,   // allows to select a file
        ENUM,        // combo box, must use setEnumValues
        COLOR,
        DATE
    };

    JsonStringWidget(wxWindow * parent);

    void           from_schema(nlohmann::json const & J ) override;
    nlohmann::json to_json() const override;
    void           setValue(nlohmann::json const & J) override;

    void setStyle( jsonStringStyle s);
    void setEnumValues( std::vector<std::string> const & values);
};


/**
 * @brief The JsonBooleanWidget class
 *
 * Handles all boolean related values. There is only one style
 * which is a checkbox.
 */
class JsonBooleanWidget : public JsonValue, public wxWindow
{
    wxBoxSizer* m_sizer;
    wxWindow *m_widget = nullptr;
public:

    JsonBooleanWidget(wxWindow * parent);

    void from_schema(nlohmann::json const & J ) override;
    void            setValue(nlohmann::json const & J) override;
    nlohmann::json to_json() const override;
};

/**
 * @brief The JsonArrayWidget class
 *
 * Handles all array related json values. It handles two styles
 * FIXED_SIZE (default) and GROWABLE. If set to GROWABLE
 * the widget will display two additional buttons to add
 * or remove values from the array.
 */
class JsonArrayWidget : public JsonValue, public wxWindow
{
    wxChoice                       *m_ItemsBox=nullptr;
    std::vector< JsonValueWidget* > m_jsonWidgets;
    wxBoxSizer *m_ItemSizer=nullptr;
    wxBoxSizer *m_AddRemove=nullptr;
    wxButton  *m_Add=nullptr;
    wxButton  *m_Remove=nullptr;
public:

    enum jsonArrayStyle
    {
        FIXED_SIZE,
        GROWABLE
    };

    JsonArrayWidget(wxWindow * parent);


    void           from_schema(nlohmann::json const & Ob ) override;
    nlohmann::json to_json() const override;
    void            setValue(nlohmann::json const & J) override;

    size_t size() const;
    void   clear();

    /**
     * @brief setAdditional
     * @param J
     *
     * Set the schema for additional items added to the
     * array using the +/- buttons.
     */
    void setAdditional(nlohmann::json const & J);

    /**
     * @brief popBack
     *
     * remove the last widget in the array
     */
    void popBack();

    /**
     * @brief pushBack
     * @param J
     *
     * push a VALUE to the end of the array
     */
    void pushBack(nlohmann::json const &J);
    void pushBack( JsonValueWidget*  ob );

protected:
    nlohmann::json m_additionalItems;

    void onAdd(wxCommandEvent& event);
    void onRemove(wxCommandEvent& event);

};

/**
 * @brief The JsonStringWidget class
 *
 * This class handles all string related json values.
 * It comes in a variety of styles, the default being TEXTBOX
 */
class JsonKeyWidget : public wxWindow
{
    wxStaticText * m_text;
    wxTextCtrl   * m_textEdit;
    wxButton     * m_removeButton;
    std::string m_key; // the actual key value
    std::string m_label;

    bool m_editable = false;
public:
    JsonKeyWidget(wxWindow * parent);

    std::string getValue() const;

    void setEditable(bool f);
    void setDeletable(bool f);

    void setValue(const std::string key);
    void setLabel(const std::string label);

    wxButton* getRemoveButton()
    {
        return m_removeButton;
    }
};


/**
 * @brief The JsonObjectWidget class
 *
 * Handles the object type
 */
class JsonObjectWidget : public JsonValue, public wxWindow
{
    struct prop
    {
        JsonKeyWidget   * m_key;
        JsonValueWidget  * m_value;
    };

    wxFlexGridSizer *m_sizer;
    wxChoice        *m_combobox;

    std::map< std::string, prop > m_jsonWidgets;
    std::vector< nlohmann::json > m_schemas;

    wxButton                     *m_addProperty;
    nlohmann::json                m_additionalProperties;

    wxChoice  *m_ItemsBox;
public:

    void showAdditionalPropertiesChoice(bool t)
    {
        m_ItemsBox->Show(t);
    }
    void showAddPropertyButton(bool t)
    {
        m_addProperty->Show(t);
    }
    void showOneOfChoice(bool t)
    {
        m_combobox->Show(t);
    }
    /**
     * @brief insert
     * @param label
     * @param tc1
     *
     * Insert a new widget. If the widget already exists,
     * it will delete it
     */
    void insert(const std::string key, JsonValueWidget* tc1, const std::string label, bool editable=false, bool deletable=false);

    void clear();
    /**
     * @brief erase
     * @param key
     *
     * Erase a widget by key-value
     */
    void erase(const std::string key);

    void insert(const std::string key, nlohmann::json const &J, const std::string label="");

    void showProperty(std::string key, bool sh);

    JsonObjectWidget(wxWindow * parent);

    void setValue(nlohmann::json const & J) override;
    void from_schema(nlohmann::json const & Ob ) override;
    void set_oneOf(nlohmann::json const & Ob);
    nlohmann::json to_json() const override;

    void insertAdditional(nlohmann::json const & value, std::string key);

    void handlerFuncName(wxCommandEvent& event);

    void handlerAddProperty(wxCommandEvent& event);

    void onRemoveKey(wxCommandEvent & event);
};


/**
 * @brief The JsonValueWidget class
 *
 * A JsonValueWidget is a dynamic json item that can change
 * from number, string, array, object, bool
 */
class JsonValueWidget : public JsonValue, public wxWindow
{
public:
        enum JsonType
        {
            JNULL,
            BOOLEAN,
            NUMBER,
            STRING,
            ARRAY,
            OBJECT
        };

        JsonValueWidget(wxWindow * parent);

        /**
         * @brief getBaseWidget
         * @return
         *
         * Returns the underlying widget type as a pair. Both
         * values are the same, but are cast as different pointers.
         *
         * You can cast the wxWindow* to any of the following
         * using dynamic_cast< X* >:
         *   - JsonArrayWidget
         *   - JsonBooleanWidget
         *   - JsonObjectWidget
         *   - JsonNumberWidget
         *   - JsonStringWidget
         *
         * You can then use the individual methods to convert them
         * into different representation.
         */
        std::pair< JsonValue*, wxWindow*> getBaseWidget() const;


        /**
         * @brief getType
         * @return
         *
         * Returns the current widget type:
         *
         *   - JNULL == nullptr
         *   - ARRAY == JsonArrayWidget
         *   - BOOLEAN == JsonBooleanWidget
         *   - OBJECT == JsonObjectWidget
         *   - NUMBER == JsonNumberWidget
         *   - STRING == JsonStringWidget
         */
        JsonType getType() const;
        void     setType(JsonType t );


        /**
         * @brief setValue
         * @param J
         *
         * Sets the value of the widget using a json object.
         * If the widget is currently of a differnt type, it
         * will be converted
         */
        void            setValue(nlohmann::json const & J) override;


        /**
         * @brief to_json
         * @return
         *
         * Returns the current value of of the widget in a
         * json format.
         */
        nlohmann::json to_json() const override;

        void from_schema(nlohmann::json const & Ob ) override;
protected:

        JsonType _type = JNULL;
        std::pair<JsonValue*, wxWindow*> m_widget;
};

#endif
