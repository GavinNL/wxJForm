/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wx/datectrl.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>

#include <nlohmann/json.hpp>

JsonStringWidget::JsonStringWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    wxBoxSizer *hbox = new wxBoxSizer(wxVERTICAL);
    SetSizer(hbox);
    Centre();

    setStyle(jsonStringStyle::TEXTBOX);
}


nlohmann::json JsonStringWidget::to_json() const
{
    if( auto s = dynamic_cast<wxTextCtrl*>(m_widget))
    {
        return s->GetValue().ToStdString();// s->GetLabelText().ToStdString();
    }
    else if (auto s = dynamic_cast<wxColourPickerCtrl*>(m_widget) )
    {
        auto c = s->GetColour();
        auto U = c.GetAsString(wxC2S_HTML_SYNTAX);
        // char out[10];
        // sprintf(out, "#%08x", U);
        return std::string(U);
    }
    else if( auto s = dynamic_cast<wxChoice*>(m_widget))
    {
        return s->GetStringSelection().ToStdString();
    }
    else if( auto s = dynamic_cast<wxFilePickerCtrl*>(m_widget) )
    {
        return s->GetPath().ToStdString();
    }
    else if( auto s = dynamic_cast<wxDirPickerCtrl*>(m_widget) )
    {
        return s->GetPath().ToStdString();
    }
    else if( auto s = dynamic_cast<wxDatePickerCtrl*>(m_widget))
    {
        auto d = s->GetValue();
        return d.FormatISODate().ToStdString();
    }

    return "";
}

void JsonStringWidget::setStyle( jsonStringStyle s)
{
    if( m_widget )
    {
        m_widget->Destroy();
        m_widget = nullptr;
    }
    switch( s )
    {
        case TEXTBOX:
            m_widget = new wxTextCtrl(this, -1);
            break;
        case DIR_PATH:
            m_widget = new wxDirPickerCtrl(this, -1);
            break;
        case FILE_PATH:
            m_widget = new wxFilePickerCtrl(this, -1);
            break;
        case ENUM:
            m_widget = new wxChoice(this,-1);
            break;
        case COLOR:
            m_widget = new wxColourPickerCtrl(this, -1, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_USE_TEXTCTRL | wxCLRP_SHOW_ALPHA);
            break;
        case DATE:
            m_widget = new wxDatePickerCtrl(this,-1);
            break;
    }
    GetSizer()->Add(m_widget, 0, wxEXPAND, 0);
}

void JsonStringWidget::setEnumValues( std::vector<std::string> const & values)
{
    setStyle(ENUM);
    auto S = dynamic_cast<wxChoice*>(m_widget);
    for(uint32_t i=0;i<values.size();i++)
    {
        S->Insert( values[i], i );
    }
}

void JsonStringWidget::setValue( nlohmann::json const & J)
{
    auto s = J.get<std::string>();
    if( auto S = dynamic_cast<wxChoice*>(m_widget) )
    {
        auto siz = S->GetCount();
        for(uint32_t i=0;i<siz;i++)
        {
            if( S->GetString(i) == s)
            {
                S->SetSelection(i);
                break;
            }

        }
    }
    else if( auto S = dynamic_cast<wxTextCtrl*>(m_widget) )
    {
        S->SetValue(s);
    }
    else if( auto S = dynamic_cast<wxDirPickerCtrl*>(m_widget) )
    {
        S->SetDirName( wxFileName(s) );
    }
    else if( auto S = dynamic_cast<wxFilePickerCtrl*>(m_widget) )
    {
        S->SetFileName( wxFileName(s) );
    }
    else if( auto S = dynamic_cast<wxColourPickerCtrl*>(m_widget) )
    {
        //wxColour B(100,100,100,100);
        wxColour def;
        def.Set(s);
        S->SetColour(def);
    }
}




void JsonStringWidget::from_schema(const nlohmann::json &J)
{
    auto type = J.at("type").get<std::string>();
    auto widget = J.count("ui:widget") ? J.at("ui:widget").get<std::string>() : std::string("string");

    if( type == "string")
    {
        if( J.count("enum"))
        {
            auto d = J.count("default") ? J.at("default").get<std::string>() : std::string();
            auto values = J.at("enum").get< std::vector< std::string > >();
            setEnumValues(values);
            setValue(d);
        }
        else if( widget == "date")
        {
            setStyle(JsonStringWidget::DATE);
        }
        else if( widget == "dir")
        {
            setStyle(JsonStringWidget::DIR_PATH);
        }
        else if( widget == "file")
        {
            setStyle(JsonStringWidget::FILE_PATH);
        }
        else if( widget == "color" || widget=="colour")
        {
            setStyle(JsonStringWidget::COLOR);
        }
        else
        {
            setStyle(JsonStringWidget::TEXTBOX);
        }

        if( J.count("default") )
        {
            setValue( J.at("default"));
        }
    }

}
