/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>

#include <nlohmann/json.hpp>


JsonValueWidget::JsonValueWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    m_widget.first  = nullptr;
    m_widget.second = nullptr;

    wxBoxSizer *hbox = new wxBoxSizer(wxVERTICAL);

    SetSizer(hbox);
    Centre();
}

std::pair< JsonValue*, wxWindow*> JsonValueWidget::getBaseWidget() const
{
    return m_widget;
}

void JsonValueWidget::from_schema(const nlohmann::json &schema)
{
    if( ! (schema.count("type") && schema.at("type").is_string()) )
    {
        std::cerr << "Cannot find \"type\" value. Is this JSON Document a proper JSON Schema?" << std::endl;
        return;
    }
    auto type = schema.at("type").get<std::string>();

    if( type == "boolean")
    {
        setType(JsonType::BOOLEAN);
        dynamic_cast<JsonBooleanWidget*>(getBaseWidget().second)->from_schema(schema);
    }
    else if( type == "number" ||  type == "integer")
    {
        setType(JsonType::NUMBER);
        dynamic_cast<JsonNumberWidget*>(getBaseWidget().second)->from_schema(schema);
    }
    else if( type == "string")
    {
        setType(JsonType::STRING);
        dynamic_cast<JsonStringWidget*>(getBaseWidget().second)->from_schema(schema);
    }
    else if( type == "array")
    {
        setType(JsonType::ARRAY);
        dynamic_cast<JsonArrayWidget*>(getBaseWidget().second)->from_schema(schema);
    }
    else if( type == "object")
    {
        setType(JsonType::OBJECT);
        dynamic_cast<JsonObjectWidget*>(getBaseWidget().second)->from_schema(schema);
    }
}

nlohmann::json JsonValueWidget::to_json() const
{
    if( !m_widget.first)
        return {};

    switch( getType() )
    {
        case JNULL   :
            return {};
        case BOOLEAN :
            return dynamic_cast<JsonBooleanWidget*>(getBaseWidget().second)->to_json();
        case NUMBER  :
            return dynamic_cast<JsonNumberWidget*>(getBaseWidget().second)->to_json();
        case STRING  :
            return dynamic_cast<JsonStringWidget*>(getBaseWidget().second)->to_json();
        case ARRAY   :
            return dynamic_cast<JsonArrayWidget*>(getBaseWidget().second)->to_json();
        case OBJECT  :
            return dynamic_cast<JsonObjectWidget*>(getBaseWidget().second)->to_json();
    }

    return m_widget.first->to_json();
}

JsonValueWidget::JsonType JsonValueWidget::getType() const
{
    return _type;
}

void JsonValueWidget::setType(JsonValueWidget::JsonType  t )
{
    if( getType() != t)
    {
        if( m_widget.second)
        {
            m_widget.second->Destroy();
            m_widget.first  = nullptr;
            m_widget.second = nullptr;
        }
        _type = t;
        switch( t )
        {
            case JNULL   :
                break;
            case BOOLEAN :
                {
                    auto x = new JsonBooleanWidget(this);
                    m_widget.first = x;
                    m_widget.second = x;
                }
                break;
            case NUMBER  :
                {
                    auto x = new JsonNumberWidget(this);
                    m_widget.first = x;
                    m_widget.second = x;
                }
                break;
            case STRING  :
                {
                    auto x = new JsonStringWidget(this);
                    m_widget.first = x;
                    m_widget.second = x;
                }
                break;
            case ARRAY   :
                {
                    auto x = new JsonArrayWidget(this);
                    m_widget.first = x;
                    m_widget.second = x;
                }
                break;
            case OBJECT  :
                {
                    auto x = new JsonObjectWidget(this);
                    m_widget.first = x;
                    m_widget.second = x;
                }
                break;
        }
        if( m_widget.second)
            GetSizer()->Add(m_widget.second, 0, wxEXPAND, 0);
    }
}

void JsonValueWidget::setValue(nlohmann::json const & t )
{
    if( t.is_object() )
    {
        setType(OBJECT);
        dynamic_cast<JsonObjectWidget*>(getBaseWidget().first)->setValue(t);
        dynamic_cast<JsonObjectWidget*>(getBaseWidget().first)->showOneOfChoice(false);
        dynamic_cast<JsonObjectWidget*>(getBaseWidget().first)->showAddPropertyButton(false);
    }
    else if( t.is_array() )
    {
        setType(ARRAY);
        dynamic_cast<JsonArrayWidget*>(getBaseWidget().first)->setValue(t);
    }
    else if( t.is_string() )
    {
        setType(STRING);
        dynamic_cast<JsonStringWidget*>(getBaseWidget().first)->setValue(t);
    }
    else if( t.is_boolean())
    {
        setType(BOOLEAN);
        dynamic_cast<JsonBooleanWidget*>(getBaseWidget().first)->setValue(t);
    }
    else if( t.is_number() )
    {
        setType(NUMBER);
        dynamic_cast<JsonNumberWidget*>(getBaseWidget().first)->setValue(t);
    }
    else if( t.is_null() )
    {
        setType(JNULL);
    }

}
