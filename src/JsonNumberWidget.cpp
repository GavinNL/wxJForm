/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>

#include <nlohmann/json.hpp>


JsonNumberWidget::JsonNumberWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    wxBoxSizer *hbox = new wxBoxSizer(wxVERTICAL);
    SetSizer(hbox);
    Centre();

    setStyle(jsonNumberStyle::DOUBLE_SPINBOX);
}

void JsonNumberWidget::setStyle( jsonNumberStyle s)
{
    if( m_widget )
    {
        m_widget->Destroy();
        m_widget = nullptr;
    }

    switch( s )
    {
        case INT_SPINBOX:
        {
            auto x = new wxSpinCtrl(this, wxID_ANY);
            x->SetRange( std::numeric_limits<int>::min(), std::numeric_limits<int>::max() );
            m_isFloat = false;
            m_widget = x;

            break;
        }
        case INT_SLIDER:
        {
            auto x = new wxSlider(this, wxID_ANY, 0, 0, 100);
            m_isFloat = false;
            m_widget = x;
            break;
        }
        case DOUBLE_SPINBOX:
        {
            auto x = new wxSpinCtrlDouble(this, wxID_ANY);
            x->SetRange( std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max() );
            x->SetDigits(8);
            m_widget = x;
            m_isFloat = true;
            break;
        }
        case DOUBLE_SLIDER:
            m_widget = new wxSlider(this, wxID_ANY, 0, 0, 1e6);
            m_isFloat = true;
            break;
    }
    GetSizer()->Add(m_widget, 0, wxEXPAND, 0);
}

void JsonNumberWidget::setRange(double m, double M)
{
    minF = m;
    maxF = M;
    if( auto S = dynamic_cast<wxSpinCtrlDouble*>(m_widget))
    {
        S->SetRange(m, M);
    }
    else if( auto S = dynamic_cast<wxSpinCtrl*>(m_widget))
    {
        S->SetRange(m, M);
    }
    else if( auto S = dynamic_cast<wxSlider*>(m_widget))
    {
        if( m_isFloat )
        {
            S->SetMin(0);
            S->SetMax(1e6);

            double f = S->GetValue();
            f = std::clamp(f, minF, maxF);
            auto fp = f-minF / (maxF-minF);
            S->SetValue( fp * 1e6 );
        }
        else
        {
            S->SetRange(m,M);
        }
    }
}

void JsonNumberWidget::setValue(const nlohmann::json &J)
{
    if( J.is_number() )
    {
        if( auto S = dynamic_cast<wxSpinCtrlDouble*>(m_widget))
        {
            S->SetValue( J.get<double>() );
        }
        else if( auto S = dynamic_cast<wxSpinCtrl*>(m_widget))
        {
            S->SetValue( J.get<int32_t>() );
        }
        else if( auto S = dynamic_cast<wxSlider*>(m_widget))
        {
            if( m_isFloat )
            {
                auto f = J.get<double>();
                f = std::clamp(f, minF, maxF);
                auto fp = f-minF / (maxF-minF);
                S->SetValue( fp * 1e6 );
            }
        }
    }
}

void JsonNumberWidget::from_schema(const nlohmann::json &J)
{
    auto type = J.at("type").get<std::string>();
    auto widget = J.count("ui:widget") ? J.at("ui:widget").get<std::string>() : std::string("updown");

    if( type == "integer")
    {
        if( widget == "updown")
        {
            setStyle(INT_SPINBOX);
            auto m = J.count("minimum") ? J.at("minimum").get<int32_t>() : std::numeric_limits<int32_t>::lowest();
            auto M = J.count("maximum") ? J.at("maximum").get<int32_t>() : std::numeric_limits<int32_t>::max();
            setRange(m, M);
        }
        else if(widget == "range")
        {
            setStyle(INT_SLIDER);
            auto m = J.count("minimum") ? J.at("minimum").get<int32_t>() : 0;
            auto M = J.count("maximum") ? J.at("maximum").get<int32_t>() : 100;
            setRange(m, M);
        }
    }
    else if (type == "number")
    {
        if( widget == "updown")
        {
            setStyle(DOUBLE_SPINBOX);
            auto m = J.count("minimum") ? J.at("minimum").get<int32_t>() : std::numeric_limits<float>::lowest();
            auto M = J.count("maximum") ? J.at("maximum").get<int32_t>() : std::numeric_limits<float>::max();
            setRange(m, M);
        }
        else if(widget == "range")
        {
            setStyle(DOUBLE_SLIDER);
            auto m = J.count("minimum") ? J.at("minimum").get<double>() : 0.0;
            auto M = J.count("maximum") ? J.at("maximum").get<double>() : 100;
            setRange(m, M);
        }
    }
    if( J.count("default") )
        setValue(J.at("default"));

}

nlohmann::json JsonNumberWidget::to_json() const
{
    if( auto s = dynamic_cast<wxSlider*>(m_widget))
    {
        if( m_isFloat)
        {
            double v = static_cast<double>( s->GetValue() );
            double V = static_cast<double>( s->GetMax() - s->GetMin() );

            double t = v / V;

            return minF * (1.0-t) + maxF*t;
        }
        return s->GetValue();
    }
    else if( auto s = dynamic_cast<wxSpinCtrlDouble*>(m_widget))
    {
        return s->GetValue();
    }
    else if( auto s = dynamic_cast<wxSpinCtrl*>(m_widget))
    {
        return s->GetValue();
    }
    return 0.0f;
}
