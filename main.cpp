/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#include <fstream>
#include <limits>
#include <thread>
#include <queue>

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/thread.h>

#include <nlohmann/json.hpp>
#include <lyra/lyra.hpp>

#include <wxJForm/wxJForm.h>

bool closeOnOkay=false;
bool pretty=false;
bool printJson=false;
bool _export=false;
std::string prefix = "";

void printJsonBash(nlohmann::json & J, std::string prefix)
{
    if( J.is_object() )
    {
        for(auto it = J.begin(); it != J.end() ; it++)
        {
            auto key = it.key();
            auto & val = it.value();

            if( val.is_string() )
            {
                std::cout << prefix << key << "=\"" << val.get<std::string>() << "\"" << std::endl;
            }
            else if( val.is_number_integer() )
            {
                std::cout << prefix << key <<  "=" << val.get<int>() << std::endl;
            }
            else if( val.is_number_float() )
            {
                std::cout << prefix << key <<  "=" << val.get<double>() << std::endl;
            }
            else if( val.is_number_unsigned() )
            {
                std::cout << prefix << key <<  "=" << val.get<uint32_t>() << std::endl;
            }
            else if( val.is_boolean())
            {
                std::cout << prefix << key <<  "=" << (val.get<bool>() ? std::string("true") : std::string("false")) << std::endl;
            }
            else
            {
                auto new_prefix = prefix + key + "_";
                printJsonBash(val, new_prefix);
            }
        }
    }
    else if (J.is_array())
    {
        uint32_t i=0;
        for(auto & val : J)
        {
            if( val.is_string() )
            {
                std::cout << prefix << i << "=\"" << val.get<std::string>() << "\"" << std::endl;
            }
            else if( val.is_number_integer() )
            {
                std::cout << prefix << i << "=" << val.get<int>() << std::endl;
            }
            else if( val.is_number() )
            {
                std::cout << prefix << i <<"=" << val.get<double>() << std::endl;
            }
            else if( val.is_number_unsigned() )
            {
                std::cout << prefix << i << "=" << val.get<uint32_t>() << std::endl;
            }
            else if( val.is_boolean())
            {
                std::cout << prefix << i << "=" << (val.get<bool>() ? std::string("true") : std::string("false")) << std::endl;
            }
            else
            {
                auto new_prefix = prefix + std::to_string(i) + "_";
                printJsonBash(val, new_prefix);
            }
            i++;
        }
    }
}



class MyFrame : public wxFrame
{
    JsonValueWidget            *m_root;
    wxBoxSizer                 *m_sizer;
    wxScrolledWindow           *m_scrolledWindow;
    std::thread                 m_thread;
    std::queue<nlohmann::json>  m_queue;
    wxTimer                     m_timer;
    bool                        m_quit=false;

    wxButton                    *m_CloseBtn=nullptr;
    wxButton                    *m_OkBtn   =nullptr;

public:


    /**
     * @brief MyFrame
     * @param J
     * @param schema
     *
     * Build the UI using a json object. If schema is set to true, it will build
     * the UI using the JSON Scheme. Otherwise it will show the default
     * json values.
     */
    MyFrame( nlohmann::json const & J, bool schema=false) : wxFrame(NULL, wxID_ANY, "wxJForm", wxDefaultPosition)

    {
        InitLayout();

        if( schema)
        {
            m_root->from_schema(J);
            m_OkBtn->Bind(wxEVT_BUTTON, &MyFrame::OnButton, this);
            m_CloseBtn->Bind(wxEVT_BUTTON, &MyFrame::OnButtonCancel, this);
        }
        else
        {
            m_root->setValue(J);

        }


        this->Layout();

        auto s = this->GetSize();
        if( J.count("ui:width") )  s.SetWidth( J.at("ui:width").get<int>() );
        if( J.count("ui:height") ) s.SetHeight( J.at("ui:height").get<int>() );
        if( J.count("title") )
        {
            this->SetTitle( J.at("title").get< std::string>() );
        }

        this->SetSize(s);
    }


    // Timer to read the values from the queue
    // and update the widgets
    void OnTimer(wxTimerEvent& event)
    {
        (void)event;

        while( m_queue.size() > 1 )
        {
            m_queue.pop();
        }
        if( m_queue.size())
        {
            m_root->setValue(m_queue.front());
            this->Layout();
            m_queue.pop();
        }
    }


    MyFrame(std::istream & in)  : wxFrame(NULL, wxID_ANY, "wxJForm")
    {
        InitLayout();

        m_OkBtn->Bind(wxEVT_BUTTON, &MyFrame::OnButtonCancel, this);
        m_CloseBtn->Bind(wxEVT_BUTTON, &MyFrame::OnButtonCancel, this);

        m_OkBtn->Hide();
        m_CloseBtn->Hide();

        // Start a background thread to continuiously
        // read from stdin and place the
        // json objects into a queue
        m_thread = std::thread(
                    [&]()
                    {
                        //uint32_t i=0;
                        while( !in.eof() && !m_quit)
                        {
                            try {

                                nlohmann::json J;
                                in >> J;

                                m_queue.push( std::move(J));
                            } catch (...) {

                            }

                        }
                    }
                    );

        m_timer.Bind(wxEVT_TIMER, &MyFrame::OnTimer, this, m_timer.GetId() );
        m_timer.Start(16);    // 1 second interval
    }

private:

    void OnButton(wxCommandEvent& event)
    {
        (void)event;
        if( m_root)
        {
            auto J = m_root->to_json();
            if( printJson )
            {
                if( pretty )
                    std::cout << J.dump(4) << std::endl;
                else
                    std::cout << J << std::endl;
            }
            else
            {
                printJsonBash(J, _export ? "export " + prefix : prefix);
            }
        }
        if( closeOnOkay)
            this->Close(true);
    }
    void OnButtonCancel(wxCommandEvent& event)
    {
        (void)event;

        this->Close(true);
    }

    void InitLayout()
    {
        wxBoxSizer* bSizer1;
        bSizer1 = new wxBoxSizer( wxVERTICAL );

        auto m_scrolledWindow1 = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
        m_scrolledWindow1->SetScrollRate( 5, 5 );
        m_scrolledWindow = m_scrolledWindow1;
        wxBoxSizer* bSizer4;
        bSizer4 = new wxBoxSizer( wxVERTICAL );

        m_sizer = bSizer4;

        m_root = new JsonValueWidget(m_scrolledWindow);
        m_sizer->Add(m_root, 0, wxALL|wxEXPAND, 2);

        m_scrolledWindow1->SetSizer( bSizer4 );
        m_scrolledWindow1->Layout();
        bSizer4->Fit( m_scrolledWindow1 );
        bSizer1->Add( m_scrolledWindow1, 1, wxEXPAND | wxALL, 5 );

        {
            wxBoxSizer* bSizer8;
            bSizer8 = new wxBoxSizer( wxHORIZONTAL );


            bSizer8->Add( 0, 0, 1, wxEXPAND, 5 );

            m_CloseBtn = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
            bSizer8->Add( m_CloseBtn, 0, wxALL, 5 );

            m_OkBtn = new wxButton( this, wxID_ANY, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
            bSizer8->Add( m_OkBtn, 0, wxALL, 5 );




            bSizer1->Add( bSizer8, 0, wxEXPAND, 5 );

        }

        this->SetSizer( bSizer1 );

        this->Layout();
    }

    // wxWindowBase interface
public:
    bool Destroy()
    {
        m_quit = true;
        if( m_thread.joinable())
            m_thread.join();
        return wxFrame::Destroy();
    }
};






class MyApp : public wxApp
{
public:
    virtual bool OnInit()
    {
        int width = -1;
        int height = -1;
        bool view=false;
        bool show_help=false;

        auto cli = lyra::help(show_help)
            | lyra::opt( view )
                  ["-v"]["--view"]
                  ("Read continuously read standard input and display the values"
                   ""
                   "\n\n           curl -s http://api.icndb.com/jokes/random | ./wxJForm --view \n\n"
                   ""
                   "")
            | lyra::opt( width , "width")
                ["-w"]["--width"]
                ("Width override. Sets the width of the window")
            | lyra::opt( height, "height" )
                ["-h"]["--height"]
                ("Height override. Sets the height of the window")
            | lyra::opt( printJson )
                ["-j"]["--json"]
                ("Print the output as JSON instead of environment variables")
            | lyra::opt( pretty )
                ["-p"]["--pretty"]
                ("Print the json in a pretty format instead of on a single line")
            | lyra::opt( _export )
                ["-e"]["--export"]
                ("Export the environment variables. Ignored if --json is used")
            | lyra::opt( prefix, "prefix" )
                ["--prefix"]
                ("Prepends the prefix string to the environment variables")
            | lyra::opt( closeOnOkay )
                  ["-c"]
                  ("Print out the JSON document and close the window when the user pressed OK")
            ;

        auto result = cli.parse( { argc, argv } );
        if ( !result )
        {
            std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
            exit(1);
        }
        if( show_help)
        {
            std::cout << cli << std::endl;
            exit(0);
        }

        if( !view )
        {
            nlohmann::json J;
            try
            {
                std::cin >> J;
            } catch (nlohmann::json::parse_error & e) {
                std::cerr << "Error: " << e.what() << std::endl;
                exit(1);
            }

            // Dereference all references
            for(auto it = J.begin(); it!=J.end(); it++)
            {
                if( it.key() != "$refs" && it.key() != "$defs")
                {
                    JsonValue::dereference(it.value(), J);
                }
            }
            try
            {
                auto f = new MyFrame(J, true);
                auto S = f->GetSize();
                if( width != -1)
                    S.SetWidth(width);
                if( height != -1)
                    S.SetHeight(height);
                f->SetSize(S);
                f->Show(true);
            } catch (std::exception & E) {
                std::cerr << "Error Generating the UI Schema: " << E.what() << std::endl;
                return false;
            }
        }
        else
        {
            auto f = new MyFrame(std::cin);

            auto S = f->GetSize();
            if( width != -1)
                S.SetWidth(width);
            if( height != -1)
                S.SetHeight(height);
            f->SetSize(S);
            f->Show(true);
        }

        return true;
    }
};

IMPLEMENT_APP_CONSOLE(MyApp)
