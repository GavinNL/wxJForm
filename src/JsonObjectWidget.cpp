/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>
#include <chrono>
#include <nlohmann/json.hpp>



JsonKeyWidget::JsonKeyWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    auto *panel = this;//new wxPanel(this, -1);

    m_removeButton = new wxButton(this, -1, "-");
    m_text         = new wxStaticText( this, -1, "");
    m_textEdit     = new wxTextCtrl(this, -1);


#if 0
    wxFlexGridSizer *fgs = new wxFlexGridSizer(1, 3, 1, 1);
    fgs->AddGrowableCol(0, 2);

    fgs->Insert(0, m_removeButton, 0,wxEXPAND | wxALL);
    fgs->Insert(1, m_text        , 1);
    fgs->Insert(2, m_textEdit    , 1);
#else
    wxBoxSizer *fgs  = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *fgsh = new wxBoxSizer(wxHORIZONTAL);


    m_removeButton->SetSize(    wxSize( 35, 35) );
    m_removeButton->SetMaxSize( wxSize( 35, 35) );
    m_removeButton->SetMinSize( wxSize( 35, 35) );
    m_removeButton->SetToolTip("Remove this key");
    //pSizer->Add(new wxButton(this, idMainMenu_Opt1, "Option 1", wxDefaultPosition, wxSize(150,35) ), wxALIGN_CENTER);

    fgsh->Add(m_removeButton, 0, wxALIGN_CENTER );
    fgsh->Add(m_text        , 1);
    fgsh->Add(m_textEdit    , 1);

    fgs->Add( fgsh, 0);
    fgs->Add(    0, 1);
#endif


    panel->SetSizer(fgs);

    setEditable(false);
    Centre();
}

std::string JsonKeyWidget::getValue() const
{
    if( m_textEdit->IsShown())
        return m_textEdit->GetValue().ToStdString();
    return m_key;
}

void JsonKeyWidget::setValue(const std::string key)
{
    m_key = key;
    m_text->SetLabelText(key);
    m_textEdit->SetValue(key);
}

void JsonKeyWidget::setLabel(const std::string label)
{
    m_text->SetLabelText(label);
}

void JsonKeyWidget::setEditable(bool f)
{
    m_text->Show( !f );
    m_textEdit->Show( f );
}


void JsonKeyWidget::setDeletable(bool f)
{
    m_removeButton->Show(f);
}


JsonObjectWidget::JsonObjectWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    auto *panel = this;//new wxPanel(this, -1);

    wxBoxSizer *hbox = new wxBoxSizer(wxVERTICAL);

    m_combobox = new wxChoice(this, wxID_ANY);

    m_combobox->Bind( wxEVT_CHOICE, &JsonObjectWidget::handlerFuncName, this);

    //m_combobox->Insert("test2", 0);
    //m_combobox->Insert("test", 1);
    wxFlexGridSizer *fgs = new wxFlexGridSizer(1, 2, 9, 25);

    fgs->AddGrowableCol(1, 1);

    hbox->Add( m_combobox, 0 ,wxEXPAND );
    hbox->Add(fgs, 1, wxALL | wxEXPAND, 15);

    {
        m_ItemsBox    = new wxChoice(this, wxID_ANY);
        m_addProperty = new wxButton(this, wxID_ANY, "New");
        m_addProperty->Bind( wxEVT_BUTTON, &JsonObjectWidget::handlerAddProperty, this);

        wxBoxSizer* bSizer3;
        bSizer3 = new wxBoxSizer( wxHORIZONTAL );

        bSizer3->Add(0, 1,wxALL);
        bSizer3->Add(m_ItemsBox   , 0, wxALL, 5);
        bSizer3->Add(m_addProperty, 0, wxALL, 5);

        hbox->Add(bSizer3, 0 ,wxEXPAND);
    }

    hbox->Add( 0, 10, 0, wxALL, 5 );
    panel->SetSizer(hbox);

    Centre();
    m_sizer = fgs;

}

void JsonObjectWidget::clear()
{
    for(auto & f : m_jsonWidgets)
    {
        f.second.m_key->Destroy();
        f.second.m_value->Destroy();
    }
    m_jsonWidgets.clear();
}

void JsonObjectWidget::handlerAddProperty(wxCommandEvent &event)
{
    if( m_additionalProperties.is_array() )
    {
        auto j = m_ItemsBox->GetSelection();

       // auto jv = new JsonValueWidget(this);

        //jv->from_schema(m_additionalProperties.at(j));

        static int i=0;

        auto label = std::string("key_") + std::to_string(i);
        while(true)
        {
            i++;
            if( m_jsonWidgets.count(label) == 0)
                break;
        }

        insertAdditional( m_additionalProperties.at(j), "");
        //insert(label, jv, label, true);
        //
        //wxWindow* parent=GetParent();
        //while(parent != nullptr)
        //{
        //    parent->Layout();
        //    parent = parent->GetParent();
        //}
    }
}

void JsonObjectWidget::insertAdditional(nlohmann::json const & value, std::string key)
{

    if( value.count("key") )
    {
        auto k = value.at("key").get<std::string>();

        if( m_jsonWidgets.find(k) == m_jsonWidgets.end())
        {
            auto jv = new JsonValueWidget(this);
            jv->from_schema(value);
            insert(k, jv, k, false, true);
        }
    }
    else
    {
        static int i=0;
        auto label = std::string("key_") + std::to_string(i);
        while(true)
        {
            i++;
            if( m_jsonWidgets.count(label) == 0)
                break;
        }

        auto jv = new JsonValueWidget(this);
        jv->from_schema(value);
        insert(label, jv, label, true, true);
    }

    wxWindow* parent=GetParent();
    while(parent != nullptr)
    {
        parent->Layout();
        parent = parent->GetParent();
    }
}



void JsonObjectWidget::handlerFuncName(wxCommandEvent &E)
{
    clear();
    set_oneOf(m_schemas.at( m_combobox->GetSelection()));
}

void JsonObjectWidget::erase(const std::string key)
{
    auto f = m_jsonWidgets.find(key);
    if( f != m_jsonWidgets.end() )
    {
        f->second.m_key->Destroy();
        f->second.m_value->Destroy();
        m_jsonWidgets.erase(f);
    }
}

void JsonObjectWidget::insert(const std::string key, JsonValueWidget *tc1, const std::string label, bool editable, bool deletable)
{
    erase(key);

    auto * thetitle = new JsonKeyWidget(this);

    thetitle->setEditable(editable);
    thetitle->setDeletable(deletable);

    thetitle->setValue(key);
    thetitle->setLabel(label);

    m_sizer->SetRows( m_sizer->GetRows() +1);
    m_sizer->Add(thetitle, 0, wxEXPAND);
    m_sizer->Add(tc1, 1, wxEXPAND);

    thetitle->getRemoveButton()->Bind( wxEVT_BUTTON, &JsonObjectWidget::onRemoveKey, this);
    m_jsonWidgets[key] = {thetitle, tc1};
}

void JsonObjectWidget::onRemoveKey(wxCommandEvent & event)
{
    for(auto & m : m_jsonWidgets)
    {
        if( m.second.m_key->getRemoveButton() == event.GetEventObject() )
        {
            erase(m.first);

            wxWindow* parent=GetParent();
            while(parent != nullptr)
            {
                parent->Layout();
                parent = parent->GetParent();
            }
            return;
        }
    }
    //std::cout << event.GetEventObject() << std::endl;;
}

void JsonObjectWidget::insert(const std::string key, nlohmann::json const &J, const std::string label)
{
    auto jv = new JsonValueWidget(this);
    jv->setValue(J);
    insert(key, jv, label);
}

void JsonObjectWidget::setValue(const nlohmann::json &J)
{
    if( J.is_object() )
    {
        for(auto it = J.begin(); it != J.end(); it++)
        {
            auto key = it.key();
            auto & value = it.value();

            auto f = m_jsonWidgets.find(key);
            if( f == m_jsonWidgets.end() )
            {
                insert(key, value, key);
            }
            else
            {
                f->second.m_value->setValue(value);
            }
        }
    }
}

void JsonObjectWidget::set_oneOf(nlohmann::json const & Ob)
{
    auto & J = Ob.at("properties");

    std::map< std::string, std::tuple<std::string, JsonValueWidget*, bool> > _w;

    for(auto it = J.begin(); it != J.end(); it++)
    {
        auto key = it.key();
        auto & value = it.value();

        auto jv = new JsonValueWidget(this);
        jv->from_schema(value);
        auto label = key;
        if( value.count("title") )
        {
            label  = value.at("title").get<std::string>();
        }
        bool visible = value.count("ui:visible") ? value.at("ui:visible").get<bool>() : true;
        _w[key] = {label, jv, visible};
    }

    // insert the values which are ordered
    if( Ob.count("ui:order") )
    {
        auto keys = Ob.at("ui:order").get< std::vector<std::string> >();
        for(auto & k : keys)
        {
            auto f = _w.find(k);
            if( f != _w.end())
            {
                insert( f->first, std::get<1>(f->second), std::get<0>(f->second));
                showProperty( f->first, std::get<2>(f->second) );
                _w.erase(f);
            }
        }
    }

    // insert the remaining values
    for(auto & k : _w)
    {
        insert( k.first, std::get<1>(k.second) , std::get<0>(k.second));
        showProperty( k.first, std::get<2>(k.second) );
    }

    if( Ob.count("additionalProperties"))
    {
        auto & ap = Ob.at("additionalProperties");

        if( ap.is_object() )
        {
            m_additionalProperties = nlohmann::json::array();
            m_additionalProperties.push_back(ap);
        }
        else if( ap.is_array() )
        {
            m_additionalProperties = Ob.at("additionalProperties");
        }

        int i=0;
        for(auto & J : m_additionalProperties)
        {
            std::string title = "Item_" + std::to_string(i);
            if( J.count("title") && J.at("title").is_string() )
                title = J.at("title").get<std::string>();
            m_ItemsBox->Insert(title, i);
            i++;
        }

        m_ItemsBox->SetSelection(0);
        showAddPropertyButton(true);
        if( i==1)
            showAdditionalPropertiesChoice(false);
    }
    else
    {
        showAddPropertyButton(false);
        showAdditionalPropertiesChoice(false);
    }


    wxWindow* parent=GetParent();
    while(parent != nullptr)
    {
        parent->Layout();
        parent = parent->GetParent();
    }
}

void JsonObjectWidget::from_schema(const nlohmann::json &Ob)
{
    this->SetAutoLayout(true);
    m_schemas.clear();
    if( Ob.count("oneOf"))
    {
        m_combobox->Clear();
        unsigned int i=0;
        if( Ob.at("oneOf").is_array() )
        {
            for(auto & x : Ob.at("oneOf"))
            {
                std::string title = x.at("title").get<std::string>();
                m_combobox->Insert(title, i++);
                m_schemas.push_back(x);
            }
            m_combobox->SetSelection(0);
        }

        if( Ob.at("oneOf").size() > 1)
        {
            showOneOfChoice(true);
        }
        set_oneOf( m_schemas.front() );
    }
    else
    {
        showOneOfChoice(false);
        set_oneOf(Ob);
    }
}

void JsonObjectWidget::showProperty(std::string key, bool sh)
{
    m_jsonWidgets.at(key).m_key->Show(sh);
    m_jsonWidgets.at(key).m_value->Show(sh);
}

nlohmann::json JsonObjectWidget::to_json() const
{
    nlohmann::json J;
    for(auto s : m_jsonWidgets)
    {
        auto k = s.second.m_key->getValue();
        J[k] = s.second.m_value->to_json();
    }
    return J;
}










