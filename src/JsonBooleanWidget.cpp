/*
 * Copyright 2020 GavinNL
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/clrpicker.h>
#include <wx/filepicker.h>
#include <wx/collpane.h>
#include <wxJForm/wxJForm.h>

#include <fstream>
#include <limits>

#include <nlohmann/json.hpp>

JsonBooleanWidget::JsonBooleanWidget(wxWindow *parent) : wxWindow(parent, wxID_ANY)
{
    wxBoxSizer *hbox = new wxBoxSizer(wxVERTICAL);
    SetSizer(hbox);
    Centre();
    m_widget = new wxCheckBox(this, -1, "");
    GetSizer()->Add(m_widget, 0, wxEXPAND, 0);
}

void JsonBooleanWidget::from_schema(const nlohmann::json &J)
{
    auto type = J.at("type").get<std::string>();
    auto widget = J.count("ui:widget") ? J.at("ui:widget").get<std::string>() : std::string("string");

    if( J.count("default"))
    {
        setValue(J.at("default"));
    }
    //if( type == "boolean")
    //{
    //    m_widget = new wxCheckBox(this, -1, "");
    //}

    //m_sizer->Add(m_widget, 0,   wxALL | wxEXPAND );
    //m_widget->SetToolTip( J.count("description") ? J.at("description").get<std::string>() : std::string() );
}

void JsonBooleanWidget::setValue(const nlohmann::json &J)
{
    if( J.is_boolean() )
    {
        auto v = J.get<bool>();
        dynamic_cast<wxCheckBox*>(m_widget)->SetValue(v);
    }
}

nlohmann::json JsonBooleanWidget::to_json() const
{
    if( auto s = dynamic_cast<wxCheckBox*>(m_widget))
    {
        return s->IsChecked();
    }

    return "";
}
